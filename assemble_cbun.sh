#!/bin/bash

mkdir assemble
cd assemble
mkdir cbun_jpct_example
cp ../bundle.xml ./cbun_jpct_example
cp ../app/build/outputs/apk/debug/app-debug.apk ./cbun_jpct_example/jpct_engine.apk
tar --exclude='*.DS_Store' -cvzf  jpct_example.cbun -C ./cbun_jpct_example .
cp jpct_example.cbun ../
cd ..
rm -rf assemble
 
