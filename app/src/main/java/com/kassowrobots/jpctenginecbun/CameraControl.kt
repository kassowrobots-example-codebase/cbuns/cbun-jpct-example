package com.kassowrobots.jpctenginecbun

import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import com.threed.jpct.Camera
import com.threed.jpct.SimpleVector
import kotlin.math.sqrt

// softness of the camera control gestures, ie. small numbers lead to high sensitivity
private const val CAMERA_TILT_SOFTNESS = 750f
private const val CAMERA_PAN_SOFTNESS = 750f
private const val CAMERA_ZOOM_SOFTNESS = 250f

// 0 means no inertia at all, 1 means infinite inertia
private const val CAMERA_INERTIA_RATIO = 0.95f

// boundaries for the zoom (no units)
private const val CAMERA_ZOOM_MIN = 4f
private const val CAMERA_ZOOM_MAX = 25f

// boundaries for the tilt (in radians)
private const val CAMERA_TILT_MIN = -(Math.PI / 2 - 0.01).toFloat()
private const val CAMERA_TILT_MAX = (Math.PI / 2 - 0.01).toFloat()

/**
 * This class is responsible for spherical positioning of the camera based on the user interaction,
 * ie. the camera can be moved on a sphere while it always looks to the center of the sphere. It
 * supports slide gestures for pan and tilt configuration and pinch to zoom.
 */
class CameraControl : OnTouchListener {

    // the center of the camera sphere
    private  val cameraPivotPosition = SimpleVector.create(0f, -0.1f, 0f)

    // first pointer info
    private var firstPointerID = MotionEvent.INVALID_POINTER_ID
    private var currentX1 = 0f
    private var currentY1 = 0f
    private var lastX1 = 0f
    private var lastY1 = 0f

    // second pointer info
    private var secondPointerID = MotionEvent.INVALID_POINTER_ID
    private var currentX2 = 0f
    private var currentY2 = 0f
    private var lastX2 = 0f
    private var lastY2 = 0f

    // zoom values
    private var zoomPower = 1f
    private var zoom = 6f

    // pan values
    private var panPower = 0f
    private var pan = 0f

    // tilt values
    private var tiltPower = 0f
    private var tilt = -0.2f

    /**
     * Handles on touch events on the view.
     */
    override fun onTouch(view: View, event: MotionEvent): Boolean {

        val pointerIndex: Int = event.actionIndex
        val pointerId: Int = event.getPointerId(pointerIndex)

        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                // first touch on screen detected
                firstPointerID = pointerId
                currentX1 = event.getX(firstPointerID)
                currentY1 = event.getY(firstPointerID)
                lastX1 = currentX1
                lastY1 = currentY1
            }
            MotionEvent.ACTION_UP -> {
                // last touch on screen removed
                firstPointerID = MotionEvent.INVALID_POINTER_ID
                secondPointerID = MotionEvent.INVALID_POINTER_ID
                view.performClick()
            }
            MotionEvent.ACTION_POINTER_DOWN -> {
                if (event.pointerCount == 2) {
                    // second touch on screen detected
                    secondPointerID = pointerId
                    currentX2 = event.getX(secondPointerID)
                    currentY2 = event.getY(secondPointerID)
                    lastX2 = currentX2
                    lastY2 = currentY2
                }
            }
            MotionEvent.ACTION_POINTER_UP -> {
                if (pointerId == firstPointerID) {
                    // first touch on screen removed
                    val newPointerIndex = if (pointerIndex == 0) 1 else 0
                    firstPointerID = event.getPointerId(newPointerIndex)
                    currentX1 = event.getX(newPointerIndex)
                    currentY1 = event.getY(newPointerIndex)
                    lastX1 = currentX1
                    lastY1 = currentY1
                    secondPointerID = MotionEvent.INVALID_POINTER_ID
                } else if (pointerId == secondPointerID) {
                    // second touch on screen removed
                    if (event.pointerCount > 2) {
                        val newPointerIndex = if (pointerIndex == 1) 2 else 1
                        secondPointerID = event.getPointerId(newPointerIndex)
                        currentX2 = event.getX(newPointerIndex)
                        currentY2 = event.getY(newPointerIndex)
                        lastX2 = currentX2
                        lastY2 = currentY2
                    } else {
                        secondPointerID = MotionEvent.INVALID_POINTER_ID
                    }
                }
            }
            MotionEvent.ACTION_MOVE -> {
                if (firstPointerID != MotionEvent.INVALID_POINTER_ID) {
                    val firstPointerIndex: Int = event.findPointerIndex(firstPointerID)
                    currentX1 = event.getX(firstPointerIndex)
                    currentY1 = event.getY(firstPointerIndex)
                }
                if (secondPointerID != MotionEvent.INVALID_POINTER_ID) {
                    val secondPointerIndex: Int = event.findPointerIndex(secondPointerID)
                    currentX2 = event.getX(secondPointerIndex)
                    currentY2 = event.getY(secondPointerIndex)
                }
            }
        }

        return true
    }

    /**
     * Updates camera frame (position + orientation) based on the previous touch events.
     */
    fun updateCameraFrame(camera: Camera?) {

        // calculate camera zoom
        val requestedZoomPower = calculateZoomPower()
        val zoom = recalculateZoom(requestedZoomPower)

        // calculate camera pan
        val requestedPanPower = calculatePanPower()
        val pan = recalculatePan(requestedPanPower)

        // calculate camera tilt
        val requestedTiltPower = calculateTiltPower()
        val tilt = recalculateTilt(requestedTiltPower)

        // calculate camera position (on sphere) from pan and tilt values
        val position = SimpleVector.create(1f, 0f, 0f)
        position.scalarMul(zoom)
        position.rotateZ(tilt)
        position.rotateY(pan)
        position.add(cameraPivotPosition)
        camera?.position = position
        camera?.lookAt(cameraPivotPosition)

        // save current pointer coordinates for next iteration
        lastX1 = currentX1
        lastY1 = currentY1
        lastX2 = currentX2
        lastY2 = currentY2
    }

    /**
     * Calculates camera zoom power, ie. the power used to zoom the camera. Power of 1 means no
     * zoom update. Less than 1 represents zoom in, more than 1 represents zoom out.
     */
    private fun calculateZoomPower(): Float {
        return if (secondPointerID != MotionEvent.INVALID_POINTER_ID) {
            // two or more fingers on screen -> calculate zoom from pointers distance update
            val currentDistance = sqrt(((currentX1 - currentX2) * (currentX1 - currentX2) + (currentY1 - currentY2) * (currentY1 - currentY2)).toDouble()).toFloat()
            val previousDistance = sqrt(((lastX1 - lastX2) * (lastX1 - lastX2) + (lastY1 - lastY2) * (lastY1 - lastY2)).toDouble()).toFloat()
            1 + (previousDistance - currentDistance) / CAMERA_ZOOM_SOFTNESS
        } else if (firstPointerID != MotionEvent.INVALID_POINTER_ID) {
            // one finger on screen -> no zoom update
            1f
        } else {
            // no fingers on screen -> no zoom update
            1f
        }
    }

    /**
     * Calculates camera pan power, ie. the power used to pan the camera. Power of 0 means no pan
     * update. Positive and negative values represent pan in different directions.
     */
    private fun calculatePanPower(): Float {
        return if (secondPointerID != MotionEvent.INVALID_POINTER_ID) {
            // two or more fingers on screen -> no pan
            0f
        } else if (firstPointerID != MotionEvent.INVALID_POINTER_ID) {
            // one finger on screen -> calculate tilt from pointer X update
            val pointerDeltaX = currentX1 - lastX1
            -pointerDeltaX/CAMERA_PAN_SOFTNESS
        } else {
            // no fingers on screen -> not pan
            0f
        }
    }

    /**
     * Calculates camera tilt power, ie. the power used to tilt the camera. Power of 0 means no tilt
     * update. Positive and negative values represent tilt in different directions.
     */
    private fun calculateTiltPower(): Float {
        return if (secondPointerID != MotionEvent.INVALID_POINTER_ID) {
            // two or more fingers on screen -> no tilt
            0f
        } else if (firstPointerID != MotionEvent.INVALID_POINTER_ID) {
            // one finger on screen -> calculate tilt from pointer Y update
            val pointerDeltaY = currentY1 - lastY1
            -pointerDeltaY/CAMERA_TILT_SOFTNESS
        } else {
            // no fingers on screen -> not tilt
            0f
        }
    }

    /**
     * Recalculates zoom by applying the requested zoom power.
     *
     * @param requestedPower zoom power: 1 means no update, less than 1 - zoom in, more than 1 - zoom out
     */
    private fun recalculateZoom(requestedPower: Float): Float {

        // adjust requested zoom power to simulate inertia
        zoomPower = if (requestedPower != 0f) {
            requestedPower
        } else {
            1 + (zoomPower - 1) * 0.9f
        }

        // update zoom
        zoom *= zoomPower

        // clamp zoom
        if (zoom < CAMERA_ZOOM_MIN) {
            zoom = CAMERA_ZOOM_MIN
        } else if (zoom > CAMERA_ZOOM_MAX) {
            zoom = CAMERA_ZOOM_MAX
        }

        return zoom
    }

    /**
     * Recalculates pan by applying the requested pan power.
     *
     * @param requestedPower pan power: 0 means no update, sign affects the direction
     */
    private fun recalculatePan(requestedPower: Float): Float {

        // adjust requested pan power to simulate inertia
        if (requestedPower != 0f) {
            panPower = requestedPower
        } else {
            panPower *= CAMERA_INERTIA_RATIO // simulate pan inertia
        }

        // calculate pan
        pan += panPower

        return pan
    }

    /**
     * Recalculates tilt by applying the requested tilt power.
     *
     * @param requestedPower tilt power: 0 means no update, sign affects the direction
     */
    private fun recalculateTilt(requestedPower: Float): Float {

        // adjust requested tilt power to simulate inertia
        if (requestedPower != 0f) {
            tiltPower = requestedPower
        } else {
            tiltPower *= CAMERA_INERTIA_RATIO
        }

        // calculate tilt
        tilt += tiltPower

        // clamp tilt
        if (tilt < CAMERA_TILT_MIN) {
            tilt = CAMERA_TILT_MIN
        } else if (tilt > CAMERA_TILT_MAX) {
            tilt = CAMERA_TILT_MAX
        }

        return tilt
    }

}