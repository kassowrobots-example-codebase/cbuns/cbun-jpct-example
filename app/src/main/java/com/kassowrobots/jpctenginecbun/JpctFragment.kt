package com.kassowrobots.jpctenginecbun

import android.opengl.GLSurfaceView
import android.os.Bundle
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.GestureDetectorCompat
import com.kassowrobots.api.app.fragment.KRFragment
import com.threed.jpct.*
import com.threed.jpct.util.BitmapHelper
import java.util.*
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10


class JpctFragment : KRFragment(), View.OnTouchListener {

    // surface view serves the JPCT engine as a rendering view
    private lateinit var surfaceView: GLSurfaceView

    // camera control is responsible for camera movement
    private val cameraControl = CameraControl()

    private lateinit var gestureDetector: GestureDetectorCompat

    /**
     * Called to let the fragment to instantiate its view, ie. this method is responsible for
     * generating the fragment view.
     *
     * @param inflater Allows view inflating - not used.
     * @param container Parent view container - not used.
     * @param savedInstanceState Persistent state instance - not used.
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // instantiate the GLSurfaceView as the root view of the fragment
        surfaceView = GLSurfaceView(context)

        // configure the surface view properties
        surfaceView.setEGLContextClientVersion(2)
        surfaceView.preserveEGLContextOnPause = true

        // set the surface view renderer as the JPCT scene renderer
        surfaceView.setRenderer(sceneRenderer)

        // delegate the touch handling to the camera control
        surfaceView.setOnTouchListener(this)

        gestureDetector = GestureDetectorCompat(requireContext(), sceneRenderer)

        return surfaceView
    }

    /**
     * Called when the fragment is no more resumed.
     */
    override fun onPause() {
        super.onPause()
        surfaceView.onPause()
    }

    /**
     * Called when the fragment is visible to the user and actively running.
     */
    override fun onResume() {
        super.onResume()
        surfaceView.onResume()
    }

    override fun onTouch(view: View, event: MotionEvent): Boolean {
        if (gestureDetector.onTouchEvent(event)) {
            return true
        }
        return cameraControl.onTouch(view, event)
    }

    private data class Coordinates2D(val x: Int, val y: Int)

    /**
     * Scene renderer is responsible for rendering the JPCT scene into the surface view.
     */
    private val sceneRenderer = object : GLSurfaceView.Renderer, SimpleOnGestureListener() {

        // background color of the JPCT world
        private val BACKGROUND_COLOR = RGBColor(226, 230, 240)

        // frame buffer to be used for the rendering of the JPCT world
        private var frameBuffer : FrameBuffer? = null

        // JPCT world representation
        private var world: World? = null

        private var singleTapCoordinates: Coordinates2D? = null

        private var longPressCoordinates: Coordinates2D? = null

        /**
         * Called when the surface is created - not used.
         */
        override fun onSurfaceCreated(p0: GL10?, p1: EGLConfig?) {

        }

        /**
         * Called when the surface is changed, ie. once it is created or once the device orientation
         * is updated. This is where the JPCT world should be created.
         */
        override fun onSurfaceChanged(p0: GL10?, width: Int, height: Int) {

            // configure frame buffer dimensions
            configureFrameBuffer(width, height)

            // configure world (if not already configured)
            var worldInstance = world
            if (worldInstance == null) {

                // create new world
                worldInstance = World()

                // configure world lighting (ambient + point light)
                configureWorldLighting(worldInstance)

                // generate floor grid
                generateFloorGrid(worldInstance)

                // add palette object
                addPalette(worldInstance)

                // assign world instance
                world = worldInstance
            }

        }

        /**
         * Called periodically by the surface view rendering loop. This method is responsible for
         * updating the world and mainly rendering the world to the frame buffer.
         */
        override fun onDrawFrame(p0: GL10?) {

            // recalculate camera position
            cameraControl.updateCameraFrame(world?.camera)

            // add box object on click event
            singleTapCoordinates?.let {

                // add a new box to the world below the finger position
                addBox(it)

                // clear the click event, since it was already processed
                singleTapCoordinates = null
            }

            longPressCoordinates?.let {

                // remove a box from the world bellow the finger position
                removeBox(it)

                // clear the long press event, since it was already processed
                longPressCoordinates = null
            }

            // render scene to frame buffer
            frameBuffer?.clear(BACKGROUND_COLOR)
            world?.renderScene(frameBuffer)
            world?.draw(frameBuffer)
            frameBuffer?.display()

        }

        override fun onSingleTapUp(event: MotionEvent): Boolean {
            singleTapCoordinates = Coordinates2D(event.x.toInt(), event.y.toInt())
            return true
        }

        override fun onLongPress(event: MotionEvent)  {
            longPressCoordinates = Coordinates2D(event.x.toInt(), event.y.toInt())
        }

        /**
         * Configures frame buffer dimensions.
         */
        private fun configureFrameBuffer(aWidth: Int, aHeight: Int) {
            var frameBufferInstance = frameBuffer
            if (frameBufferInstance == null) {
                // create new buffer
                frameBufferInstance = FrameBuffer(aWidth, aHeight)
                frameBufferInstance.setVirtualDimensions(
                    frameBufferInstance.width,
                    frameBufferInstance.height
                )
                frameBuffer = frameBufferInstance
            } else {
                // resize existing buffer
                frameBufferInstance.resize(aWidth, aHeight)
                frameBufferInstance.setVirtualDimensions(aWidth, aHeight)
            }
        }

        /**
         * Configures world lighting (both ambient and point).
         */
        private fun configureWorldLighting(world: World) {
            // configure ambient light
            world.setAmbientLight(150, 150, 150)

            // configure point light
            val sun = Light(world)
            sun.setIntensity(150f, 150f, 150f)
            sun.position = SimpleVector(0.0, -100.0, -100.0)
        }

        /**
         * Generates floor grid by adding the primitive plane object and applying semi transparent
         * grid texture.
         */
        private fun generateFloorGrid(world: World) {
            // add grid texture to JPCT engine
            if (!TextureManager.getInstance().containsTexture("grid")) {
                // load texture from drawable resources
                val gridTexture = Texture(
                    BitmapHelper.rescale(
                        BitmapHelper.convert(ResourcesCompat.getDrawable(resources, R.drawable.grid, null)),
                        1024,
                        1024
                    )
                )
                // register texture to TextureManager
                TextureManager.getInstance().addTexture("grid", gridTexture)
            }

            // generate plane object
            val grid = Primitives.getPlane(1, 5f)

            // add object to the world
            world.addObject(grid)

            // rotate the plane to the horizontal position
            grid.rotateX(1.57f)

            // enable transparency (since the grid texture is partially transparent)
            grid.transparency = 3

            // set texture (by the name used during the registration)
            grid.setTexture("grid")

            grid.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS)

            // compile and build the object
            grid.compile()
            grid.build()
        }

        /**
         * Add palette object to the world by loading it from the OBJ and MTL files.
         */
        private fun addPalette(world: World) {
            // load all 3D objects from the OBJ and MTL files
            val partList = Loader.loadOBJ(
                resources.openRawResource(R.raw.scene_obj),
                resources.openRawResource(R.raw.scene_mtl), 1f
            )
            // list the 3D objects to find the palette (by its name)
            for (object3D in partList) {
                val st = StringTokenizer(object3D.name, "_")
                val name = st.nextToken()
                if (name == "palette") {
                    // rotation has to be applied, since the OBJ file uses different coordinate system
                    object3D.rotateX((-1.0 * Math.PI).toFloat())
                    object3D.rotateMesh()
                    object3D.rotationMatrix = Matrix()

                    // add the palette object to the world
                    world.addObject(object3D)

                    // enable check collisions mode
                    object3D.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS)

                    // compile and build the object
                    object3D.compile()
                    object3D.build()
                }
            }
        }

        /**
         * Adds a new box to the world bellow the finger position.
         *
         * This method casts a ray from the camera along the direction based on the touch
         * coordinates. It checks the world for a collision with this ray and if the collision
         * occurs, the collision 3D coordinates are calculated. Finally the box is generated and
         * placed at the collision coordinates.
         *
         * @param touchCoordinates 2D coordinates of the touch event
         */
        private fun addBox(touchCoordinates: Coordinates2D) {

            val collisionCoordinates = calcCollisionCoordinates(touchCoordinates)
            collisionCoordinates?.let {
                // generate box object
                val box = Primitives.getBox(0.25f, 1f)

                // add the box to the world
                world?.addObject(box)

                // adjust the collision position by the half of the box height (since the box origin is in the center of the box).
                collisionCoordinates.add(SimpleVector.create(0f, -0.25f, 0f))

                // translate the box from the origin by the calculated vector of the collision
                box.translate(collisionCoordinates)
                box.translateMesh()
                box.translationMatrix = Matrix()

                // rotate the box (since it is placed diagonally rotated by default)
                box.rotateY((Math.PI/4).toFloat())

                // enable check collisions mode
                box.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS)

                // compile and build the box
                box.compile()
                box.build()
            }

        }

        /**
         * Removes a box from the world bellow the finger position.
         *
         * This method casts a ray from the camera along the direction based on the touch
         * coordinates. It checks the world for a collision with this ray and if the collision
         * occurs, it returns the object that caused the collision. If such object is recognized as
         * a box (based on its id), it is removed from the world.
         *
         * @param touchCoordinates 2D coordinates of the touch event
         */
        private fun removeBox(touchCoordinates: Coordinates2D) {

            // find the projection from the camera to the world space for the given 2D coordinates
            val direction = Interact2D.reproject2D3DWS(world?.camera, frameBuffer, touchCoordinates.x, touchCoordinates.y)

            // find the closest object that collides with the ray casted from camera along the calculated direction
            val result = world?.calcMinDistanceAndObject3D(world?.camera?.position, direction, 10000f)

            // obtain the object as the second item of the result array (the first item represents distance to the object)
            val object3D = result?.get(1) as Object3D?

            // if any object was found, check it's id and possibly remove it
            object3D?.let {
                // if the object is not grid (0) or palette (1), remove it from the world
                if (object3D.id > 1) {
                    world?.removeObject(object3D)
                }
            }
        }

        /**
         * Calculates collision 3D coordinates from the touch 2D coordinates.
         *
         * This method casts a ray from the camera along the direction based on the touch 2D
         * coordinates. It checks the world for a collision with this ray and if the collision
         * occurs, the collision 3D coordinates are calculated.
         *
         * @param touchCoordinates 2D coordinates of the touch event
         */
        private fun calcCollisionCoordinates(touchCoordinates: Coordinates2D) : SimpleVector? {

            // find the projection from the camera to the world space for the given 2D coordinates
            val direction = Interact2D.reproject2D3DWS(world?.camera, frameBuffer, touchCoordinates.x, touchCoordinates.y).normalize()

            // calculate distance to the closest polygon in the world following the given direction
            val distance = world?.calcMinDistance(world?.camera?.position, direction, 10000f)

            distance?.let {
                // multiply the direction by the calculated distance
                direction?.scalarMul(distance)

                // add camera position do direction vector to get world space coordinates of the collision
                direction?.add(world?.camera?.position)

                return direction
            }

            return null
        }

    }

}